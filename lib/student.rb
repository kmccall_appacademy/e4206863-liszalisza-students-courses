class Student
  attr_accessor :first_name, :last_name

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{first_name} #{last_name}"
  end

  def courses
    # Return a list of the Courses in which the student is enrolled
    @courses
  end

  def enroll(course)
    # Take a Course object, add it to the student's list of courses, and update the Course's list of enrolled students.
    raise "The course you're trying to enroll in conflicts with another course time" if has_conflict?(course)
    @courses << course unless @courses.include?(course)
    course.students << self
  end

  def course_load
    # Returns a hash of departments to # of credits the student is taking in that department.
    @course_load = Hash.new(0)
    @courses.each do |course|
      @course_load[course.department] += course.credits
    end
    @course_load
  end

  def has_conflict?(new_course)
    @courses.any? do |existing_course|
      new_course.conflicts_with?(existing_course)
    end
  end
end
